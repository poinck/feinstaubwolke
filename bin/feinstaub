#!/bin/bash

. ~/.sorc
if [[ ! -n "${rh_offset}" ]] ; then
    rh_offset=0
fi

# e - entry
round_and_int() {
    local e=$1

    e=${e//\"}
    e=$( echo "scale=0; (${e}+0.5)/1" | bc -l )
    if [[ "${e}" -lt 0 ]] ; then
        e=$(( e - 1 ))
    fi

    echo $e
}

calculate_soi() {
    local tmpp=$1
    local hump=$2
    local p25p=$3
    local soi=50

    tmpp=${tmpp//\"}
    hump=${hump//\"}
    p25p=${p25p//\"}

    local p25_index=$( echo "${p25p}*100/20" | bc -l )
    local p25_index_r=$( round_and_int "${p25_index}" )

    local dew=$( calculate_dewpoint "${tmpp}" "${hump}" )
    dew_index=$( echo "(${dew}-10)" | bc -l )
    local dew_index_r=$( round_and_int "${dew_index}" )
    if [[ "${dew_index_r}" -lt 0 ]] ; then
        dew_index=$( echo "${dew_index}*(-1)" | bc -l )
    fi
    dew_index=$( echo "${dew_index}*100/30" | bc -l )
    dew_index_r=$( round_and_int "${dew_index}" )

    soi=$(( p25_index_r + dew_index_r ))
    if [[ "${soi}" -gt 100 ]] ; then
        soi=100
    fi

    echo $soi
}

get_td() {
    local sensort=$1
    local nowts="$2"

    sensort=${sensort//\"/}     # remove '"'
    local td=0

    local sensorts=$( LC_ALL=C date -u -d "${sensort}" '+%s' )
    td=$(( nowts - sensorts ))

    echo ${td}
}

calculate_corrected_pm() {
    # relative humidity correction of particulate matter
    # based on paper: Antonio et al. 2018 "sensors (basel)"
    local pm=$1
    local hum=$2

    pm=${pm//\"}
    hum=${hum//\"}

    if [[ "${hum}" == "100.00" ]] ; then
        # avoid devision by zero
        hum="99.99"
    fi

    local r=0
    r=$( echo "${pm}/(1+((0.4/1.65)/(-1+(1/(${hum}/100)))))" | bc -l )

    echo "${r}"
}

calculate_dewpoint() {
    # https://www.chemie.de/lexikon/Taupunkt.html#Berechnung_von_Taupunkt_und_Frostpunkt
    # valid for t_dry from -30°C until 70°C
    local t_dry=$1
    local rh=$2

    rh=${rh//\"}
    t_dry=${t_dry//\"}

    local t_dew=0
    t_dew=$( echo "(241.2*l(${rh}/100)+((4222.03716*${t_dry})/(241.2+${t_dry})))/(17.5043-l(${rh}/100)-((17.5043*${t_dry})/(241.2+${t_dry})))" | bc -l )

    echo "${t_dew}"
}

offset() {
    local v=$1
    local o=$2

    v=${v//\"}
    o=${o//\"}
    r=$( echo "${v}+${o}" | bc -l )

    echo "${r}"
}

calculate_cloud_altitude() {
    # in meters above ground
    local t_dry=$1
    local t_dew=$2

    t_dry=${t_dry//\"}

    local c_alt=0
    # constant temperature change: 100 m / 0.976°C
    # but using henning-approximation: 0.0082 K / 1 m
    c_alt=$( echo "(${t_dry}-(${t_dew}))/0.0082" | bc -l )
    c_alt=$( echo "scale=0; ${c_alt}/1" | bc -l )

    echo "${c_alt}"
}

max_td=3600     # 1 hour in seconds
status_text=""
sensor_p10_time=""
get_latest_measurement() {
    echo "[DEBUG] boxid = ${osm_boxid}"
    sensors_json=$( curl -s -m 80 "https://api.opensensemap.org/boxes/${osm_boxid}/sensors" )

    # testing
    #echo "${sensors_json}" > ~/_tmp/latest_measurement.json
    #sensors_json=$( cat ~/_tmp/latest_measurement.json )

    sensorids=$( echo "${sensors_json}" | jq '.sensors[]._id' )
    if [[ -n "${sensorids}" ]] ; then
        t_now=$( date -u '+%s' )
        sensorid_a=(${sensorids})
        echo -e "[DEBUG] sensorid_a[#${#sensorid_a[*]}] = ${sensorid_a[@]}"

        sensortimes=$( echo "${sensors_json}" | jq '.sensors[].lastMeasurement.createdAt' )
        sensorvalues=$( echo "${sensors_json}" | jq '.sensors[].lastMeasurement.value' )

        sensortime_ai=(${sensortimes})
        sensorvalue_ai=(${sensorvalues})
        for id in ${!sensorid_a[*]} ; do
            #echo "[DEBUG] $id.${sensorid_a[$id]} = ${sensorvalue_ai[$id]}"
            if [[ "\"${osm_p10id}\"" == "${sensorid_a[$id]}" ]] ; then
                p10=${sensorvalue_ai[$id]}
                p10_td=$( get_td "${sensortime_ai[$id]}" "${t_now}" )
                echo "[DEBUG] sensortime = ${sensortime_ai[$id]}"
                sensor_p10_time_raw=${sensortime_ai[$id]//\"/}     # remove '"'
                sensor_p10_time=$( LC_ALL=C date -d "${sensor_p10_time_raw}" '+%-d. %b, %R' )
            elif [[ "\"${osm_p25id}\"" == "${sensorid_a[$id]}" ]] ; then
                p25=${sensorvalue_ai[$id]}
                p25_td=$( get_td "${sensortime_ai[$id]}" "${t_now}" )
            elif [[ "\"${osm_tmpid}\"" == "${sensorid_a[$id]}" ]] ; then
                tmp=${sensorvalue_ai[$id]}
                tmp_td=$( get_td "${sensortime_ai[$id]}" "${t_now}" )
            elif [[ "\"${osm_humid}\"" == "${sensorid_a[$id]}" ]] ; then
                hum=${sensorvalue_ai[$id]}
                hum_td=$( get_td "${sensortime_ai[$id]}" "${t_now}" )
            elif [[ "\"${osm_hpaid}\"" == "${sensorid_a[$id]}" ]] ; then
                hpa=${sensorvalue_ai[$id]}
                hpa_td=$( get_td "${sensortime_ai[$id]}" "${t_now}" )
            fi
        done

        echo -e "[DEBUG] p10.${osm_p10id} = $p10, td = ${p10_td}"
        echo -e "[DEBUG] p25.${osm_p25id} = $p25, td = ${p25_td}"
        echo -e "[DEBUG] tmp.${osm_tmpid} = $tmp, td = ${tmp_td}"
        echo -e "[DEBUG] hum.${osm_humid} = $hum, td = ${hum_td}"
        echo -e "[DEBUG] hpa.${osm_hpaid} = $hpa, td = ${hpa_td}"

        # rh offset
        hum=$( offset "${hum}" "${rh_offset}")

        dew=$( calculate_dewpoint "${tmp}" "${hum}" )
        echo "[DEBUG] dew = ${dew}"
        clt=$( calculate_cloud_altitude "${tmp}" "${dew}" )
        dew=$( round_and_int "${dew}" )
        echo "[DEBUG] clt = ${clt}"

        # correct particulate matter for relative humidity
        p10=$( calculate_corrected_pm "${p10}" "${hum}" )
        p25=$( calculate_corrected_pm "${p25}" "${hum}" )

        so_index=$( calculate_soi "${tmp}" "${hum}" "${p25}" )

        p10=$( round_and_int "$p10" )
        p25=$( round_and_int "$p25" )
        tmp=$( round_and_int "$tmp" )
        hum=$( round_and_int "$hum" )
        hpa=$( round_and_int "$hpa" )
        hpa=$(( hpa / 100 ))

    else
        echo "[DEBUG] warn: no data received"
    fi

    # display status if data is too old (> 1 hour)
    so_not_current=false
    if [[ "${p10_td}" -gt "${max_td}" ]] ; then
        so_not_current=true
    fi
    if [[ "${p25_td}" -gt "${max_td}" ]] ; then
        so_not_current=true
    fi
    if [[ "${tmp_td}" -gt "${max_td}" ]] ; then
        so_not_current=true
    fi
    if [[ "${hum_td}" -gt "${max_td}" ]] ; then
        so_not_current=true
    fi
    if [[ "${so_not_current}" == true ]] ; then
        status_text=", old"
    fi
}

dew=0
clt=0
get_latest_measurement
index_name="OK"
if [[ "${so_index}" -gt 50 ]] ; then
    index_name="NOT ok"
fi

title_text="${index_name}: ${so_index}, T: ${tmp}°C, rH: ${hum}%, ${sensor_p10_time}${status_text}"
content_text="PM¹: ${p10}, PM²: ${p25} µg/m³, Td: ${dew}°C, P: ${hpa} hPa, C: ${clt} m"
echo -e "[DEBUG] title = '${title_text}', \ncontent = '${content_text}'"

if [[ -n "${notify_mode}" ]] ; then
    if [[ "${notify_mode}" == "termux" ]] ; then
        termux-notification --id so1 --title "${title_text}" --content "${content_text}" --action "bash /data/data/com.termux/files/home/bin/feinstaub" --on-delete "bash /data/data/com.termux/files/home/bin/feinstaub" --button1 "History" --button1-action "bash /data/data/com.termux/files/home/bin/feinstaub-history"
        #--led-color "226467" --led-on 3000
    elif [[ "${notify_mode}" == "zenity" ]] ; then
        zenity --notification --text="${title_text}\n${content_text}" --window-icon=${feinstaub_path}/config/wind.svg
    elif [[ "${notify_mode}" == "lampe" ]] ; then
        echo "pm25=${p25}" > /tmp/feinstaublampe
        cat /tmp/feinstaublampe
    fi
else
    echo "notify_mode not set."
fi
