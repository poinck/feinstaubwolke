#!/bin/bash

BOXID="XxXxxxxxXXXXxXXXXXXXxXXX"

DAYS_BACK=21
DATA_DIR="/home/USER/path/to/feinstaub/data/"

SENSOR_SDS011_PM10="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_SDS011_PM25="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_DHT22_TEMP="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_DHT22_HUM="XxXxxxxxXXXXxXXXXXXXxXXX"
sensors=(
    "${SENSOR_SDS011_PM10}"
    "${SENSOR_SDS011_PM25}"
    "${SENSOR_DHT22_TEMP}"
    "${SENSOR_DHT22_HUM}"
)
sensor_names=(
    "pm10"
    "pm25"
    "temp"
    "hum"
)

# colors
#             MR:      MB:
C0='\e[30m' # *color0, *color8,  gray/
C1='\e[31m' # *color1, *color9,  orange/ (red)
C2='\e[32m' # *color2, *color10, green/
C3='\e[33m' # *color3, *color11, yellow/
C4='\e[34m' # *color4, *color12, blue/
C5='\e[35m' # *color5, *color13, purple/
C6='\e[36m' # *color6, *color14, cyan/turquoise
C7='\e[37m' # *color7, *color15, white/

CO=${C1}
CR=${C1}
CG=${C2}
CY=${C3}
CB=${C4}
CP=${C5}
CT=${C6}
CW=${C7}

# modifiers
MR='\e[0m' # reset
MB='\e[1m' # bold (lighter)
MI='\e[3m' # italic

from=$( date "+%FT%R:%S.000Z" -u -d "-${DAYS_BACK} days" )

append_new_measurements() {
    sensorid="$1"
    sensorname="$2"

    latest_measurements=$( curl -s "https://api.opensensemap.org/boxes/${BOXID}/data/${sensorid}?format=csv&from-date=${from}" )
    all_measurements=$( cat "${DATA_DIR}${sensorname}_measurements.csv" )
    new_measurements=$( echo -e "${latest_measurements}\n${all_measurements}" | sort | uniq -u )
    new_measurements_count="$( echo "${new_measurements}" | wc -l )"
    echo "found ${new_measurements_count} new ${sensorname} measurements"
    echo "${new_measurements}" >> ${DATA_DIR}${sensorname}_measurements.csv
}

echo "append_new_measurements since ${from}"
for id in ${!sensors[*]} ; do
    append_new_measurements "${sensors[$id]}" "${sensor_names[$id]}"
    sleep .23

done



