# Readme: feinstaubwolke

Backup your Luftdaten.info suspended particulate matter sensor (SPM) data as CSV using OpenSenseMap. Additionally fetch sensor data on your Android device empowered by Termux.

![fh](./history1.png)

## Content

- `bin/feinstaub-history`: show last 2 days of sensor data using a R plot. (can be used on a standard Linux system, on Android with Termux installed or even on Windows with WSL/Ubuntu installed)
- `bin/feinstaub`: display sensor data as notification on Android or Gnome desktop, or the PM 2.5 value as light color using [lampe](https://gitlab.com/poinck/lampe).
- `get_latest_measurements_of_my_sensors.sh`: retrieves last 21 days of sensor data using REST-API. Results will be in continous files for each sensor.
- `download_from_opensensemap.sh`: fetches last 21 days of sensor data using HTTPS archive. Results will be in seperate files for each day and sensor.
- `config/systemd/user/feinstaub.service`: systemd service file
- `config/systemd/user/feinstaub.timer`: systemd timer configuration for daily persistent downloads

## Setup

**configure scripts: `get_latest_measurements_of_my_sensors.sh` and/or `download_from_opensensemap.sh`**

Depending on which scripts you want to use, following variables need to be set according to your OpenSenseBox.

```.sh
BOXID="XxXxxxxxXXXXxXXXXXXXxXXX"
BOXNAME="boxname"

DAYS_BACK=21
DATA_DIR="/home/USER/path/to/feinstaub/data/"

SENSOR_SDS011_PM10="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_SDS011_PM25="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_DHT22_TEMP="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_DHT22_HUM="XxXxxxxxXXXXxXXXXXXXxXXX"
```

**scheduled backup of sensor data**

- copy the systemd service file and timer configuration to `/home/USER/.config/systemd/user/`
- edit `/home/USER/.config/systemd/user/feinstaub.service`; absolute paths to scripts need to be adjusted to your environment
- enable the timer

```.sh
systemctl --user enable feinstaub.timer
systemctl --user status feinstaub.timer
```

**Configure Termux notification script for Android**

- additional dependencies: R
- copy "`bin/feinstaub`" and "`bin/feinstaub-history`" to "~/bin/" on your Android device and create a file called "~/.sorc" with following content:

```.sh
osm_boxid="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_p10id="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_p25id="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_tmpid="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_humid="XxXxxxxxXXXXxXXXXXXXxXXX"
feinstaub_path="/data/data/com.termux/files/home/gits/feinstaubwolke/"
img_command="sxiv"    # for example
notify_mode="termux"
```

- start "./bin/feinstaub" from Termux.

**Configure Zenity notification script for Gnome Shell**

- *TODO: the history-function is currently not accessible through notification like on Android*
- additional dependencies: R (and an image programm of your choice to show history graph)
- copy "`bin/feinstaub`" and "`bin/feinstaub-history`" to "~/bin/" and create a file called "~/.sorc" with following content:

```.sh
osm_boxid="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_p10id="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_p25id="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_tmpid="XxXxxxxxXXXXxXXXXXXXxXXX"
osm_humid="XxXxxxxxXXXXxXXXXXXXxXXX"
feinstaub_path="/home/user/gits/feinstaubwolke/"
img_command="sxiv"    # for example
notify_mode="zenity"
```

- furthermore, a start-script at "config/local/share/applications/Feinstaub.desktop" is provided.

```.sh
cp config/local/share/applications/Feinstaub.desktop ~/.local/share/applications
vim ~/.local/share/applications/Feinstaub.desktop  # adapt to your local paths
```

- start Feinstaub by searching for "feinstaub" in Gnome Shell

## Connecting a LCD1602 to the Luftdaten.info sensor

*graphic changed after Luftdaten.info; source: https://luftdaten.info/feinstaubsensor-bauen/ (called 9.3.2019)*

![LCD](./luftdaten_sensor_with_lcd.png)
