#!/bin/bash

BOXID="XxXxxxxxXXXXxXXXXXXXxXXX"
BOXNAME="boxname"
WEBDAV_USERNAME="HyTbguBP4EkqBcp"

DAYS_BACK=21
DATA_DIR="/home/USER/path/to/feinstaub/data/"

SENSOR_SDS011_PM10="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_SDS011_PM25="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_DHT22_TEMP="XxXxxxxxXXXXxXXXXXXXxXXX"
SENSOR_DHT22_HUM="XxXxxxxxXXXXxXXXXXXXxXXX"

sensors=(
    "${SENSOR_SDS011_PM10}"
    "${SENSOR_SDS011_PM25}"
    "${SENSOR_DHT22_TEMP}"
    "${SENSOR_DHT22_HUM}"
)
sensor_names=(
    "pm10"
    "pm25"
    "temp"
    "hum"
)

# colors
#             MR:      MB:
C0='\e[30m' # *color0, *color8,  gray/
C1='\e[31m' # *color1, *color9,  orange/ (red)
C2='\e[32m' # *color2, *color10, green/
C3='\e[33m' # *color3, *color11, yellow/
C4='\e[34m' # *color4, *color12, blue/
C5='\e[35m' # *color5, *color13, purple/
C6='\e[36m' # *color6, *color14, cyan/turquoise
C7='\e[37m' # *color7, *color15, white/

CO=${C1}
CR=${C1}
CG=${C2}
CY=${C3}
CB=${C4}
CP=${C5}
CT=${C6}
CW=${C7}

# modifiers
MR='\e[0m' # reset
MB='\e[1m' # bold (lighter)
MI='\e[3m' # italic

check_file() {
    local filename="$1"
    local date="$2"

    grep -l "${date}" "${DATA_DIR}${filename}" > /dev/null 2>&1
    rc_grep="$?"

    if [[ "${rc_grep}" -eq 0 ]] ; then
        return 0
    else
        return 1
    fi
}

rc_curl=0

fetch_archive() {
    local date="$1"

    sensor_id=0
    for sensor in "${sensors[@]}" ; do
        url="https://uni-muenster.sciebo.de/index.php/s/${WEBDAV_USERNAME}/download?path=/data/${date}/${BOXID}-${BOXNAME}/${sensor}-${date}.csv"
        filename="${sensor_names[${sensor_id}]}-${date}.csv"
        check_file "${filename}" "${date}"
        rc_check_file="$?"
        if [[ "${rc_check_file}" -gt 0 ]] ; then
            # redownload, if CSV on disk is not valid or missing
            #echo "${url}"
            echo -n "downloading ${filename} .. "
            if [[ -z "${PRETEND}" ]] ; then
                curl -s "${url}" -o "${DATA_DIR}${filename}"
                rc_curl="$?"
            else
                echo -n "PRETEND=1 "
            fi
            check_file "${filename}" "${date}"
            rc_check_file="$?"
            if [[ "${rc_check_file}" -eq 0 ]] ; then
                echo -e "${CG}complete${MR}"
            else
                echo -e "${CO}failure${MR}"
            fi
        else
            echo -e "${filename} ${MB}ok${MR}"
        fi

        sensor_id=$(( sensor_id + 1 ))
    done
}

days_counter=${DAYS_BACK}
echo -e "${MB}${CB}download_from_opensensemap.sh:${MR} attempt to download last ${days_counter} days"
echo "data_dir = ${DATA_DIR}"

for_all_days() {
    while [[ "${days_counter}" -gt 0 ]] ; do
        date=$( date "+%Y-%m-%d" -d "-${days_counter} days" )
        fetch_archive "${date}"

        days_counter=$(( days_counter - 1 ))
    done
}

for_all_days
if [[ "${rc_curl}" -gt 0 ]] ; then
    echo -e "${CO}ERROR:${MR} last call of curl returned with exit code ${MB}${CO}${rc_curl}${MR} (!= 0) .. retry in 42s"
    sleep 42
    for_all_days
    if [[ "${rc_curl}" -gt 0 ]] ; then
        echo -e "${CO}ERROR:${MR} last call of curl returned with exit code ${MB}${CO}${rc_curl}${MR} (!= 0) .. giving up"
        exit 1
    fi
else
    echo "available downloads complete"
fi
