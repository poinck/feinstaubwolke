#!/usr/bin/Rscript

png(filename = "./rhoffset%02d.png",
    width = 3000, height = 1500, units = "px",
    pointsize = 12, res = 300, family = "TeX Gyre Heros",
    bg = "white", type = "cairo"
)

calculate_corrected_pm <- function(pm, hum) {
    # relative humidity correction of particulate matter
    # based on paper: Antonio et al. 2018 "sensors (basel)"
    r <- NA

    #cat("hum:", hum, "\n")
    if (hum == 100) {
        # avoid devision by zero
        hum <- 99.99
    }

    kappa <- .4     # efflorescence point of compound that was measured
                    # (unkown: so .4 is an assumption)
    aw <- hum / 100 # water activity RH/100

    C <- 1 + ((kappa / 1.65) / (-1 + (1 / aw)))
    r <- pm / C

    return(r)
}

# test humidity correction
yrange <- range(c(0, 10))
trh <- seq(from = 0, to = 100, by = .1)
tpm <- 10
tcpm <- NA
for (i in 1:length(trh)) {
    tcpm[i] <- calculate_corrected_pm(tpm, trh[i])
}
plot(trh, tcpm, type = "l", ylim = yrange)
tpm <- 5
tcpm <- NA
for (i in 1:length(trh)) {
    tcpm[i] <- calculate_corrected_pm(tpm, trh[i])
}
par(new = TRUE)
plot(trh, tcpm, type = "l", col = "red", axes = FALSE, ylim = yrange)
tpm <- 2.5
tcpm <- NA
for (i in 1:length(trh)) {
    tcpm[i] <- calculate_corrected_pm(tpm, trh[i])
}
par(new = TRUE)
plot(trh, tcpm, type = "l", col = "blue", axes = FALSE, ylim = yrange)
abline(h = 0, col = "darkgray", lty = 3)

rhs <- read.csv(file = "~/dateien/sonstiges/feinstaubsensor/data/hum_measurements.csv")
rhs$v <- as.numeric(as.character(rhs$value))
str(rhs)
tail(rhs)
temps <- read.csv(file = "~/dateien/sonstiges/feinstaubsensor/data/temp_measurements.csv")
#temps$d <- as.POSIXct(temps$createdAt)
temps$v <- as.numeric(as.character(temps$value))
str(temps)
tail(temps)

yrange <- range(c(-10, 100))
plot(temps$v, type = "l", ylim = yrange)
par(new = TRUE)
plot(rhs$v, type = "l", ylim = yrange,
    axes = FALSE, xlab = "", ylab = "", col = "red")
abline(h = 0, col = "darkgray", lty = 3)


min_temp <- min(temps$v, na.rm = TRUE)
cat("min_temp:", min_temp, "\n")
length(temps$v[temps$v < 0])
cnt <- sum(temps$v[temps$v < 0], na.rm = TRUE)
cnt_corrected <- cnt / (60 / 3)
cat("cnt:", cnt, ", cnt_corrected (rhoffset?):", cnt_corrected, "\n")

# using only a subset, since (new) bme280 was installed
l <- 250000
tv <- temps$v[l:length(temps$v)]

tv_min <- min(tv, na.rm = TRUE)
cat("tv_min (subset):", tv_min, "\n")
length(tv[tv < 0])
cntv <- sum(tv[tv < 0], na.rm = TRUE)
# bme280: cumulative degrees below 0 per 2 hours = %RH offset
# / 3, because sensor measures every 3 minutes (~ 180s)
cntv_corrected <- cnt / ((60 / 3) * 2.8)
cat("cntv (subset):", cntv, ", cntv_corrected (subset, rhoffset?):",
    cntv_corrected, "\n")

rhs$vo <- rhs$v + (rhs$v / 100) * cntv_corrected
par(new = TRUE)
plot(rhs$vo, type = "l", ylim = yrange,
    axes = FALSE, xlab = "", ylab = "", col = "#0000ff77")
