#!/usr/bin/Rscript

args = commandArgs(trailingOnly = TRUE)
if (length(args) == 1) {
    history_filename <- paste(args[1], "history.png", sep = "")
} else {
    history_filename <- "history.png"
}

# load configuration
rc <- read.csv(file = "~/.sorc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
#str(rc)
#print(rc)
i <- 1
found_k <- 1
rh_offset <- 0
for (k in rc$key) {
    if (k == "feinstaub_path") {
        found_k <- i
    } else if (k == "rh_offset") {
        rh_offset <- as.numeric(as.character(rc$value[i]))
    }
    #cat("k =", k, ", v =", rc$value, "\n")
    i <- i + 1
}
#cat("rh_offset:", rh_offset, "\n")
feinstaub_path <- "./"
i <- 1
for (v in rc$value) {
    if (i == found_k) {
        feinstaub_path <- v
    }
    i <- i + 1
}
#cat("feinstaub_path =", feinstaub_path, "\n")

history_png <- paste(feinstaub_path, history_filename, sep = "")
png(filename = history_png,
    width = 3000, height = 1500, units = "px",
    pointsize = 12, res = 300, family = "TeX Gyre Heros",
    bg = "white", type = "cairo"
)

# test svg
#svg(filename = paste(feinstaub_path, "history.svg", sep = ""),
#    width = 10, height = 5, bg = "white",
#    pointsize = 12
#)

t_now <- as.POSIXct(Sys.time())
attr(t_now, "tzone") <- "UTC"
#print(t_now)
t_start <- as.POSIXct(round(as.numeric(as.POSIXlt(t_now, origin = "1970-01-01")) / (10 * 60)) * (10 * 60), origin = "1970-01-01") - as.difftime(2, units = "days")
t_seq <- seq(from = t_start, by = 10 * 60, to = t_now)
t_start3 <- t_now - as.difftime(3, units = "days")
t_seq3 <- seq(from = t_start3, by = 10 * 60, to = t_now)
t_seq60mins <- seq(from = round(t_start, "hours"), by = 10 * 60 * 6, to = t_now)
#print(t_seq60mins)

so <- list("time" = t_seq, "time3" = t_seq3, "time60" = t_seq60mins)
#print(so$time)
#print(so$time3[1])
attr(so$time, "tzone") <- "UTC"
attr(so$time3, "tzone") <- "UTC"
attr(so$time60, "tzone") <- "UTC"

calculate_corrected_pm <- function(pm, hum) {
    # relative humidity correction of particulate matter
    # based on paper: Antonio et al. 2018 "sensors (basel)"
    r <- NA

    #cat("hum:", hum, "\n")
    if (hum == 100) {
        # avoid devision by zero
        hum <- 99.99
    }

    kappa <- .4     # efflorescence point of compound that was measured
                    # (unkown: so .4 is an assumption)
    aw <- hum / 100 # water activity RH/100

    C <- 1 + ((kappa / 1.65) / (-1 + (1 / aw)))
    r <- pm / C

    return(r)
}

tmp_file <- paste(feinstaub_path, "tmp.csv", sep = "")
tmp <- read.csv(tmp_file, sep = ",")
tmp <- tmp[dim(tmp)[1]:1, ]  # reverse order
#print(tmp)
hum_file <- paste(feinstaub_path, "hum.csv", sep = "")
hum <- read.csv(hum_file, sep = ",")
hum <- hum[dim(hum)[1]:1, ]  # reverse order
hum$value <- hum$value + rh_offset #- (((100 - hum$value) / rh_offset) * rh_offset)
hpa_file <- paste(feinstaub_path, "hpa.csv", sep = "")
hpa <- read.csv(hpa_file, sep = ",")
hpa <- hpa[dim(hpa)[1]:1, ]  # reverse order
p25_file <- paste(feinstaub_path, "p25.csv", sep = "")
p25 <- read.csv(p25_file, sep = ",")
p25 <- p25[dim(p25)[1]:1, ]  # reverse order
#str(p25)
for (i in 1:length(p25$value)) {
    p25$value[i] <- calculate_corrected_pm(p25$value[i], hum$value[i])
}
p10_file <- paste(feinstaub_path, "p10.csv", sep = "")
p10 <- read.csv(p10_file, sep = ",")
p10 <- p10[dim(p10)[1]:1, ]  # reverse order
for (i in 1:length(p10$value)) {
    p10$value[i] <- calculate_corrected_pm(p10$value[i], hum$value[i])
}
time_hpa <- as.POSIXct(strptime(hpa$createdAt, "%Y-%m-%dT%H:%M:%S", tz = "UTC"))
time_hum <- as.POSIXct(strptime(hum$createdAt, "%Y-%m-%dT%H:%M:%S", tz = "UTC"))
time_p25 <- as.POSIXct(strptime(p25$createdAt, "%Y-%m-%dT%H:%M:%S", tz = "UTC"))
time_p10 <- as.POSIXct(strptime(p10$createdAt, "%Y-%m-%dT%H:%M:%S", tz = "UTC"))
time <- as.POSIXct(strptime(tmp$createdAt, "%Y-%m-%dT%H:%M:%S", tz = "UTC"))

heights_file <- paste(feinstaub_path, "heights.csv", sep = "")
heights <- read.csv(heights_file, sep = ",")
box_height <- heights$height[1]

calculate_dewpoint <- function(tmp, hum) {
    dew <- NA

    if (!is.na(hum) && !is.na(tmp)) {
        dew <- ( 241.2 * log( hum / 100 ) + ( (4222.03716 * tmp ) / (241.2 + tmp ))) / (17.5043 - log( hum / 100) - ( ( 17.5043 * tmp) / ( 241.2 + tmp )))
    }

    return(dew)
}

calculate_reduced_air_pressure <- function(tmp, hum, hpa, h) {
    # based on equation of DWD (Deutscher Wetterdienst)
    r <- NA

    g0 <- 9.80665   # acceleration caused by earth gravity (m/s²)
    Ch <- 0.12      # change of water wapor pressure (K/hPa)
    Rair <- 287.05  # gas constant of dry air (m²/s²K)
    a <- 0.0065     # vertical temperature gradient (K/m)

    Th <- tmp + 273.15
    E <- hpa * (hum / 100)
    r <- hpa * exp(g0 / (Rair * (Th + Ch * E + a * (h / 2))) * h)

    return(r)
}

calculate_soiv <- function(tmp, hum, p25) {
    soi <- NA

    # 10 micrograms per cubic meter of particulate matter smaller than
    # 2.5 micrometer is considered harmful according to WHO
    # - 10 micrograms: index 50
    # - 20 micrograms: index 100
    p25_index <- p25 * 100 / 20

    # dewpoint of 10 degrees celsius is considered ideal (calculated using
    # temperature and humidity
    # - 10 degrees: index 0
    # - -5 or 25 degrees: index 50
    # - -20 or 40 degrees: index 100
    dew <- calculate_dewpoint(tmp, hum)

    dew_index <- dew - 10
    if (!is.na(dew)) {
        if (dew_index < 0) {
            dew_index <- dew_index * (-1)
        }
        dew_index <- dew_index * 100 / 30
    }

    # debug
    #cat(tmp, hum, "\n")
    #cat(p25, dew, "\n")
    #cat(p25_index, dew_index, "\n")

    soi <- p25_index + dew_index
    if (!is.na(soi)) {
        if (soi > 100) {
            soi <- 100
        }
    }

    return(soi)
}

# last measurment values
tmpv <- round(tmp$value[length(tmp$value)], digits = 0)
humv <- round(hum$value[length(hum$value)], digits = 0)
hpav <- round(hpa$value[length(hpa$value)] / 100, digits = 0)
dewv <- round(calculate_dewpoint(tmp$value[length(tmp$value)], hum$value[length(hum$value)]), digits = 0)
p10v <- round(p10$value[length(p10$value)], digits = 0)
p25v <- round(p25$value[length(p25$value)], digits = 0)
soiv <- round(calculate_soiv(tmp$value[length(tmp$value)], hum$value[length(hum$value)], p25$value[length(p25$value)]), digits = 0)
hparv <- round(calculate_reduced_air_pressure(tmp$value[length(tmp$value)], hum$value[length(hum$value)], hpa$value[length(hpa$value)] / 100, box_height),
    digits = 0)
#cat("last tmp value:", tmpv, "\n")
#cat("last hum value:", humv, "\n")
#cat("last p10 value:", p10v, "\n")
#cat("last p25 value:", p25v, "\n")

#str(time)

# shifts measurements one day in the past
# d - 10-minute-average measurements vector
# t - corresponding time series vector
shift_1day <- function(d, t) {
    # initialize resulting vector length with length of
    # incoming vector
    l <- length(t)
    #print(l)
    r <- rep(NA, l)

    for (i in c(1:l)) {
        r[i] <- d[i]
    }

    return(r)
}

# creates a data.frame with x-minute minimums
# of measurements
#  t - timestamps of measurements
#  m - measurements
# rt - x-minute time-slots
agg_xminutes_mins <- function(t, m, rt) {
    l <- length(rt)
    r <- rep(NA, l)

    rt_l <- l - 1
    tm_l <- length(t)
    tm_istart <- 1
    for (rt_i in c(1:rt_l)) {
        rt_next <- rt_i + 1
        #value_count <- 0
        for (tm_i in c(tm_istart:tm_l)) {
            if (t[tm_i] > rt[rt_i] && t[tm_i] < rt[rt_next]) {
                #value_count <- value_count + 1
                if (!is.na(r[rt_i])) {
                    r[rt_i] <- min(c(r[rt_i], m[tm_i]))
                    #r[rt_i] <- r[rt_i] * ((value_count - 1) / value_count) + m[tm_i] / value_count
                }
                else {
                    r[rt_i] <- m[tm_i]
                }
            }
            else if (t[tm_i] > rt[rt_next]) {
                break
            }
        }
        #cat(tm_i, " ")
        tm_istart <- tm_i
    }

    return(r)
}

# creates a data.frame with x-minute maximums
# of measurements
#  t - timestamps of measurements
#  m - measurements
# rt - x-minute time-slots
agg_xminutes_maxs <- function(t, m, rt) {
    l <- length(rt)
    r <- rep(NA, l)

    rt_l <- l - 1
    tm_l <- length(t)
    tm_istart <- 1
    for (rt_i in c(1:rt_l)) {
        rt_next <- rt_i + 1
        #value_count <- 0
        for (tm_i in c(tm_istart:tm_l)) {
            if (t[tm_i] > rt[rt_i] && t[tm_i] < rt[rt_next]) {
                #value_count <- value_count + 1
                if (!is.na(r[rt_i])) {
                    r[rt_i] <- max(c(r[rt_i], m[tm_i]))
                    #r[rt_i] <- r[rt_i] * ((value_count - 1) / value_count) + m[tm_i] / value_count
                }
                else {
                    r[rt_i] <- m[tm_i]
                }
            }
            else if (t[tm_i] > rt[rt_next]) {
                break
            }
        }
        #cat(tm_i, " ")
        tm_istart <- tm_i
    }

    return(r)
}

# creates a data.frame with x-minute averages
# of measurements
#  t - timestamps of measurements
#  m - measurements
# rt - x-minute time-slots
agg_xminutes_avgs <- function(t, m, rt) {
    l <- length(rt)
    r <- rep(NA, l)

    rt_l <- l - 1
    tm_l <- length(t)
    tm_istart <- 1
    for (rt_i in c(1:rt_l)) {
        rt_next <- rt_i + 1
        value_count <- 0
        for (tm_i in c(tm_istart:tm_l)) {
            if (t[tm_i] > rt[rt_i] && t[tm_i] < rt[rt_next]) {
                value_count <- value_count + 1
                if (!is.na(r[rt_i])) {
                    r[rt_i] <- r[rt_i] * ((value_count - 1) / value_count) + m[tm_i] / value_count
                }
                else {
                    r[rt_i] <- m[tm_i] / value_count
                }
            }
            else if (t[tm_i] > rt[rt_next]) {
                break
            }
        }
        #cat(tm_i, " ")
        tm_istart <- tm_i
    }

    return(r)
}

calculate_dews <- function(tmps, hums) {
    l <- length(tmps)
    #print(l)
    dews <- rep(NA, l)

    for (i in c(1:l)) {
        dews[i] <- calculate_dewpoint(tmps[i], hums[i])
    }

    return(dews)
}

calculate_so_index <- function(rt, tmp, hum, p10, p25) {
    l <- length(rt)
    so_index <- rep(NA, l)

    for (i in c(1:l)) {
        ii <- i * 6
        #cat(ii, "\n")
        so_index[i] <- calculate_soiv(tmp[ii], hum[ii], p25[i])
    }

    return(so_index)
}

so$tmp <- agg_xminutes_avgs(time, tmp$value, so$time)
#warnings()
#print(so$tmp)
so$tmp3 <- agg_xminutes_avgs(time, tmp$value, so$time3)
#print(so$tmp3)
y2 <- shift_1day(so$tmp3, so$time)
#print(length(y2))
so$hum <- agg_xminutes_avgs(time_hum, hum$value, so$time)
so$hpa <- agg_xminutes_avgs(time_hpa, hpa$value, so$time)
so$hpa <- so$hpa / 100

#print("p25")
so$p25 <- agg_xminutes_avgs(time_p25, p25$value, so$time60)
#so$p25 <- agg_xminutes_avgs(time_p25, p25$value, so$time)
#print(so$p25)

#so$p25min <- agg_xminutes_mins(time_p25, p25$value, so$time)
#so$p10min <- agg_xminutes_mins(time_p10, p10$value, so$time)
#so$p10 <- agg_xminutes_avgs(time_p10, p10$value, so$time60)
#so$p10 <- agg_xminutes_avgs(time_p10, p10$value, so$time)
#so$p25max <- agg_xminutes_maxs(time_p25, p25$value, so$time)
so$p10max <- agg_xminutes_maxs(time_p10, p10$value, so$time60)
so$soi <- calculate_so_index(so$time60, so$tmp, so$hum, so$p10, so$p25)
so$dew <- calculate_dews(so$tmp, so$hum)

#print(so)
#str(so)

p25m <- so$p25
#p25m[1] <- NA
l_p25m <- length(p25m)
p25m[seq(l_p25m, l_p25m, by = 1)] <- NA
p25_avg_maximum <- max(p25m, na.rm = TRUE)
p25_avg_maximum_r <- round(p25_avg_maximum, digits = 0)
p25_avg_maximum_i <- which.max(p25m)
#cat("p25 avg max =", p25_avg_maximum, "at i =", p25_avg_maximum_i, "\n")
#print(max(so$tmp, na.rm = TRUE))
tmpm <- so$tmp
tmpm[1:6] <- NA
l_tmpm <- length(tmpm)
tmpm[seq(l_tmpm-5, l_tmpm, by = 1)] <- NA
#print(length(tmpm))
#print(max(tmpm, na.rm = TRUE))
#print(tmpm)
tmp_avg_maximum <- max(tmpm, na.rm = TRUE)
tmp_avg_maximum_r <- round(tmp_avg_maximum, digits = 0)
tmp_avg_maximum_i <- which.max(tmpm)
#cat("tmp avg max =", tmp_avg_maximum, "at i =", tmp_avg_maximum_i, "\n")
tmpi <- so$tmp
tmpi[1:6] <- NA
l_tmpi <- length(tmpi)
tmpi[seq(l_tmpi-5, l_tmpi, by = 1)] <- NA
tmp_avg_minimum <- min(tmpi, na.rm = TRUE)
tmp_avg_minimum_r <- round(tmp_avg_minimum, digits = 0)
tmp_avg_minimum_i <- which.min(tmpi)

#print(time)
xrange <- range(c(so$time[1], so$time[length(so$time)]))
#print(xrange)
tmp_min <- min(so$tmp, na.rm = TRUE) - 2
tmp_max <- max(so$tmp, na.rm = TRUE) + 6
y2_min <- min(y2, na.rm = FALSE) - 2
y2_max <- max(y2, na.rm = FALSE) + 6
#print(y2_min)
if (is.na(y2_min)) {
    y2_min <- tmp_min
}
if (is.na(y2_max)) {
    y2_max <- tmp_max
}
yrange <- range(c(
    tmp_min, tmp_max,
    min(so$dew, na.rm = TRUE) - 2,
    max(so$dew, na.rm = TRUE) + 6,
    y2_min, y2_max
))
#print(yrange)
time_ticks <- c(round(so$time[2], "days"),
    round(so$time[length(so$time) - 2], "hours"))
#str(time_ticks)
#print(time_ticks)

# range for humidity an particulate matter
yrange_hum_pm <- range(c(0, 100))

par(mar = c(4, 7, 4, 5))
plot(so$time, y2,
    type = "l", lwd = .5, col = "white",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange,
    main = paste("station history until",
        strftime(so$time[length(so$time) - 2],
            tz = "UTC",
            format = "%a, %d. %b %Y at %k:%M"), "UTC (2 days)"),
    xaxs = "i", yaxs = "r", xlim = xrange
)
axis.POSIXct(x = so$time, format = "%a %k:%M", side = 1,
    at = seq(time_ticks[1], time_ticks[2], by = as.difftime(12, units = "hours")))

# temperature 1 day shifted
l <- length(so$tmp) - 1
for (i in c(1:l)) {
    c <- "white"
    if (!is.na(so$tmp[i]) && !is.na(y2[i])) {
        c <- "lightcyan2"
        if (so$tmp[i] > y2[i]) {
            c <- "lavenderblush2"
        }
        polygon(
            c(so$time[i], so$time[i + 1], so$time[i + 1], so$time[i]),
            c(y2[i], y2[i + 1], so$tmp[i + 1], so$tmp[i]),
            #density = 100, angle = 45,
            col = c, border = NA
        )
    }
}

# particulate matter p10 maximum and p25 mean
par(new = TRUE)
plot(so$time60, so$p10max,
    type = "S", lwd = 2, col = "lightgray",
    axes = FALSE, #verticals = FALSE,
    ylab = "", xlab = "", ylim = yrange_hum_pm,
    xaxs = "i", yaxs = "r", xlim = xrange
)
par(new = TRUE)
plot(so$time60, so$p25,
    type = "s", lwd = 1.5, #col = "#aaaaaa", #col = "#3f3f74",
    axes = FALSE, col = "#6767a1", #pch = 20,
    ylab = "", xlab = "", ylim = yrange_hum_pm,
    xaxs = "i", yaxs = "r",
    xlim = xrange
)
#label <- bquote(PM[2.5 / 10] * .(": ")~bold(.(p25v)) * .("/") * bold(.(p10v)) * .(" µg/m³"))
label = "particulate matter [µg/m³] (PM)"
mtext(label, side = 2, line = 2, col = "#6767a1")
toffset <- 3
if (p25_avg_maximum > 95) {
    toffset <- -3
}
text(x = so$time60[p25_avg_maximum_i], y = p25_avg_maximum + toffset,
    labels = p25_avg_maximum_r, font = 2, cex = .8, adj = 0,
    col = "#6767a1"
)

axis(side = 2, col = "#6767a1")

calculate_hum_color <- function(hum) {
    r <- "white"

    # from:
    # - darkorange r,g,b = 255, 140, 0; to:
    # - white r,g,b = 255, 255, 255
    if (!is.na(hum)) {
        #cr <- 255
        cg <- 140 + hum / 100 * 115
        cb <- 0 + hum / 100 * 255
        #if (hum < 50) {
        #    a <- (50 - hum) / 50
        #}
        #else {
        #    a <- (hum - 50) / 50
        #}
        r <- rgb(1, cg / 255, cb / 255, 1)
    }

    return(r)
}

calculate_so_color <- function(index) {
    r <- "white"

    # from:
    # - green/turquios (#008267) r,g,b = 0, 130, 103; to:
    # - rose/purpur (#bf004d) r,g,b = 191, 0, 77
    if (!is.na(index)) {
        cr <- 0 + index / 100 * 191
        cg <- 130 - index / 100 * 130
        cb <- 103 - index / 100 * 26
        if (index < 50) {
            a <- (50 - index) / 50
        }
        else {
            a <- (index - 50) / 50
        }
        r <- rgb(cr / 255, cg / 255, cb / 255, a)
    }

    return(r)
}

# humidity
l <- length(so$hum)
for (i in c(2:l)) {
    c <- calculate_hum_color(so$hum[i])
    #cat("c:", c, "hum:", so$hum[i], "\n")
    if (!is.na(so$hum[i]) && !is.na(so$time[i])) {
        polygon(
            c(so$time[i - 1], so$time[i],
                so$time[i], so$time[i - 1]),
            c(102.5, 102.5, 100.5, 100.5),
            col = c, border = NA
        )
    }
}

# so-index
l <- length(so$soi) - 1
for (i in c(1:l)) {
    c <- calculate_so_color(so$soi[i])
    #cat("c:", c, "so:", so$soi[i], "\n")
    polygon(
        #c(so$time60[i - 1], so$time60[i],
        #    so$time60[i], so$time60[i - 1]),
        c(so$time60[i], so$time60[i + 1],
            so$time60[i + 1], so$time60[i]),
        c(-2.5, -2.5, -0.5, -0.5),
        col = c, border = NA
    )
}
rmargin <- 12
if (tmpv < -9 || soiv > 99 || humv > 99) {
    rmargin <- 15
}
text(x = so$time[length(so$time) - rmargin], y = 94.5,
    labels = "relative humidity [%]: ", font = 1, adj = 1,
    col = "darkorange"
)
text(x = so$time[length(so$time) - rmargin], y = 95,
    labels = humv, font = 2, adj = 0,
    col = "darkorange"
)
soi_label_col <- calculate_so_color(0)
index_name <- "ok [index]: "
if (soiv > 50) {
    index_name <- "not ok [index]: "
    soi_label_col <- calculate_so_color(100)
}
text(x = so$time[length(so$time) - rmargin], y = 88.5,
    labels = index_name, font = 1, adj = 1,
    col = soi_label_col
)
text(x = so$time[length(so$time) - rmargin], y = 89,
    labels = soiv, font = 2, adj = 0,
    col = soi_label_col
)
text(x = so$time[length(so$time) - rmargin], y = 82.5,
    labels = "temperature [°C]: ", font = 1, adj = 1,
    col = "black"
)
text(x = so$time[length(so$time) - rmargin], y = 83,
    labels = tmpv, font = 2, adj = 0,
    col = "black"
)

text(x = so$time[length(so$time) - 92], y = 94.5,
    labels = "estimated dewpoint [°C]: ", font = 1, adj = 1,
    col = "darkorange"
)
text(x = so$time[length(so$time) - 92], y = 95,
    labels = dewv, font = 2, adj = 0,
    col = "darkorange"
)
text(x = so$time[length(so$time) - 92], y = 88.5,
    labels = "PM [µg/m³] (< 2.5 µm, 1h mean): ", font = 1, adj = 1,
    col = "#6767a1"
)
text(x = so$time[length(so$time) - 92], y = 89,
    labels = p25v, font = 2, adj = 0,
    col = "#6767a1"
)
text(x = so$time[length(so$time) - 92], y = 82.5,
    labels = "PM [µg/m³] (< 10 µm, 1h max): ", font = 1, adj = 1,
    col = "darkgray"
)
text(x = so$time[length(so$time) - 92], y = 77.5,
    labels = "humidity corrected particulate matter  ", font = 3, adj = 1,
    col = "darkgray", cex = .7
)
text(x = so$time[length(so$time) - 92], y = 83,
    labels = p10v, font = 2, adj = 0,
    col = "darkgray"
)
text(x = so$time[length(so$time) - 222], y = 94.5,
    labels = "air pressure [hPa]: ", font = 1, adj = 1,
    col = "darkgreen"
)
text(x = so$time[length(so$time) - 222], y = 95,
    labels = hpav, font = 2, adj = 0,
    col = "darkgreen"
)

reduce_air_pressure <- function(rt, tmp, hum, hpa, h) {
    l <- length(rt)
    r <- rep(NA, l)

    for (i in c(1:l)) {
        if (!is.na(tmp[i]) && !is.na(hum[i]) && !is.na(hpa[i])) {
            r[i] <- calculate_reduced_air_pressure(tmp[i], hum[i], hpa[i], h)
        }
    }

    return(r)
}

so$hpar <- reduce_air_pressure(so$time, so$tmp, so$hum, so$hpa, box_height)
#print(so$hpar)
#print(hparv)

text(x = so$time[length(so$time) - 222], y = 88.5,
    labels = "air pressure, ", font = 1, adj = 1,
    col = "#8fbf8f"
)
text(x = so$time[length(so$time) - 222], y = 82.5,
    labels = "reduced [hPa]: ", font = 1, adj = 1,
    col = "#8fbf8f"
)
text(x = so$time[length(so$time) - 222], y = 83,
    labels = hparv, font = 2, adj = 0,
    col = "#8fbf8f"
)

# air pressure, hpa
yrange_hpa <- range(c(
    1013, 993, 1033,
    max(so$hpa, na.rm = TRUE) + 12,
    min(so$hpa, na.rm = TRUE) - 5,
    max(so$hpar, na.rm = TRUE) + 12,
    min(so$hpar, na.rm = TRUE) - 5
))
par(new = TRUE)
plot(so$time, so$hpar,
    type = "l", lty = 1, lwd = 4.5, col = "white",
    ylab = "", xlab = "", ylim = yrange_hpa,
    xaxs = "i", yaxs = "r", xlim = xrange,
    axes = FALSE
)
par(new = TRUE)
plot(so$time, so$hpar,
    type = "l", lty = 3, lwd = 1.5, col = "#8fbf8f",
    ylab = "", xlab = "", ylim = yrange_hpa,
    xaxs = "i", yaxs = "r", xlim = xrange,
    axes = FALSE
)
par(new = TRUE)
plot(so$time, so$hpa,
    type = "l", lty = 1, lwd = 4.5, col = "white",
    ylab = "", xlab = "", ylim = yrange_hpa,
    xaxs = "i", yaxs = "r", xlim = xrange,
    axes = FALSE
)
par(new = TRUE)
plot(so$time, so$hpa,
    type = "l", lty = 3, lwd = 1.5, col = "darkgreen",
    ylab = "", xlab = "", ylim = yrange_hpa,
    xaxs = "i", yaxs = "r", xlim = xrange,
    axes = FALSE
)
axis(side = 2, line = 3, col = "darkgreen", col.ticks = "darkgreen",
    lty = 3, lwd = 1.5
)
label <- paste("air pressure [hPa] (", box_height, " m asl)", sep = "")
mtext(label,
    side = 2, line = 5, col = "darkgreen")

# dewpoint
#print(length(so$hum))
#print(length(so$time))
#print(so$dew)
par(new = TRUE)
plot(so$time, so$dew,
    # white outline for dewpoint
    type = "l", lwd = 4.5, col = "white",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange,
    xaxs = "i", yaxs = "r", xlim = xrange
)
par(new = TRUE)
plot(so$time, so$dew,
    type = "l", lwd = 1.5, col = "darkorange",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange,
    xaxs = "i", yaxs = "r", xlim = xrange
)
#axis(side = 4)
mtext("estimated dewpoint [°C]",
    side = 4, line = 3, col = "darkorange")

# temperature, black
par(new = TRUE)
plot(so$time, so$tmp,
    type = "l", lwd = 4.5, col = "white",
    ylab = "", xlab = "", ylim = yrange,
    xaxs = "i", yaxs = "r", xlim = xrange,
    axes = FALSE
)
par(new = TRUE)
plot(so$time, so$tmp,
    panel.first = {
        abline(h = 0, col = "black", lwd = .5, lty = "dotted")
        #abline(v = as.numeric(seq(time_ticks[1], time_ticks[2], by = as.difftime(24, units = "hours"))), col = "#aaaaaa", lty = "dotted")
    },
    type = "l", lwd = 1.5, col = "black",
    ylab = "", xlab = "", ylim = yrange,
    xaxs = "i", yaxs = "r", xlim = xrange,
    axes = FALSE
)
axis(side = 4, line = 0, col = "black")
#mtext(paste("temperature: ", tmpv, "°C", sep = ""),
#    side = 4, line = 2, col = "black")
mtext("temperature [°C] ",
    side = 4, line = 2, col = "black")
mtext("local time", side = 1, line = 2, col = "black")
text(x = so$time[tmp_avg_maximum_i], y = tmp_avg_maximum + 1,
    labels = tmp_avg_maximum_r, font = 2, cex = .8, adj = .5,
    #col = "#bf004d"
    col = "black"
)
text(x = so$time[tmp_avg_minimum_i], y = tmp_avg_minimum + 1,
    labels = tmp_avg_minimum_r, font = 2, cex = .8, adj = .5,
    #col = "#008267"
    col = "black"
)

#print(so$p25)
#print(so$time[length(so$time)])
#print(time[length(time)])
#print(so$hum)



